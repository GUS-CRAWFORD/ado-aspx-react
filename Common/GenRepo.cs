﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public abstract class GenRepo<Type, Ctx>
        where Type : class
        where Ctx : IGenContext, IDisposable
    {
        public GenRepo(Ctx context)
        {
            Context = context;
        }
        protected Ctx Context;
        public abstract Type Find(params object[] keys);
        public abstract IEnumerable<Type> Get();
        public abstract void Upsert(Type item);
        public abstract void Delete(Type item);
    }
    public interface IGenContext
    {
        void SaveChanges();
    }
    public class UnitOfWork<C> : IDisposable
        where C : IGenContext, IDisposable
    {
        protected readonly C context;
        public C Context { get { return context; } }

        public UnitOfWork(C injected)
        {
            context = injected == null ? (C)Activator.CreateInstance(typeof(C)) : injected;
        }
        public UnitOfWork() {
            context = (C)Activator.CreateInstance(typeof(C));
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    Context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
