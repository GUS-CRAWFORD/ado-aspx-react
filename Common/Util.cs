﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Util
    {
        public object Argument;
        /// <summary>
        /// Wrap an object in a Util
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static Util Object(object arg)
        {
            return new Util
            {
                Argument = arg
            };
        }
        /// <summary>
        /// Returns true if arg contains an Equal value for every property Argument has
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public bool IsLike(object arg)
        {
            var yourSet = arg.GetType().GetProperties();
            foreach (var property in Argument.GetType().GetProperties())
            {
                if (yourSet.Contains(property))
                {
                    if (!arg.GetType().GetProperty(property.Name).GetValue(arg).Equals(property.GetValue(Argument)))
                        return false;
                }
                else return false;
            }
            return true;
        }
        /// <summary>
        /// Returns true if arg and Argument have Equal properties and values (may not be same type, or reference)
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public bool IsEquivalentTo(object arg)
        {
            return IsLike(arg) && Object(arg).IsLike(Argument);
        }
    }
    /// <summary>
    /// A class containing some utilities around strings without having to write extentsions
    /// </summary>
    public class StringUtil : Util
    {
        public static StringUtil String(string arg)
        {
            return new StringUtil
            {
                Argument = arg
            };
        }
        /// <summary>
        /// Returns true if Argument is null or Equals string.Empty
        /// </summary>
        /// <returns></returns>
        public bool IsNullOrEmpty()
        {
            return Argument == null || ((string)Argument).Equals(string.Empty);
        }
    }
}
