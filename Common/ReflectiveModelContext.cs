﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace Common
{
    public class ReflectiveModelContext<S> : IDisposable
    {
        private DataSet dataSet;
        public DataSet DataSource { get { return dataSet; } }
        public ReflectiveModelContext()
        {
            dataSet = new DataSet(Regex.Replace(typeof(S).Name, @"(TableModel|SchemaModel|Model|Table)", ""));
            foreach (var table in typeof(S).GetFields())
            {
                try
                {
                    var tableType = GetType().GetProperty(table.Name).PropertyType;
                    GetType().GetProperty(table.Name).SetValue(this, Activator.CreateInstance(tableType, new object[] { dataSet.Tables.Add(table.Name) }));
                    foreach (var column in table.FieldType.GetProperties())
                    {
                        dataSet.Tables[table.Name].Columns.Add(column.Name, column.PropertyType);
                    }
                }
                catch (NullReferenceException) { }
            }
        }
        public Table<TableType> GetTable<TableType>() where TableType : class {
            foreach (var table in typeof(S).GetFields())
            {
                try
                {
                    var tableType = GetType().GetProperty(table.Name).PropertyType.GetGenericArguments().First();
                    if (tableType == typeof(TableType))
                        return (Table <TableType>) GetType().GetProperty(table.Name).GetValue(this);
                }
                catch (NullReferenceException) { }
            }
            return null;
        }
        public void Dispose()
        {
            ((IDisposable)DataSource).Dispose();
        }
    }
    public class Table<T> where T : class
    {
        protected readonly DataTable DataTable;
        public List<T> ToList()
        {
            var items = new List<T>();
            foreach (DataRow row in DataTable.Rows)
            {
                var values = new List<object>();
                var item = Activator.CreateInstance(typeof(T));
                foreach (DataColumn column in DataTable.Columns)
                {
                    item.GetType().GetProperty(column.ColumnName).SetValue(item, row[column]);
                }
                items.Add(item as T);
            }
            return items;
        }
        public Table(DataTable table)
        {
            DataTable = table;
        }
        public Table<T> Add(T item)
        {
            var values = new List<object>();
            foreach (var column in item.GetType().GetProperties())
            {
                values.Add(column.GetValue(item));
            }
            DataTable.Rows.Add(values.ToArray());
            return this;
        }
        public Table<T> Add(List<T> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
            return this;
        }
    }
}
