﻿using FileManager.Data.DomainModels;
using FileManager.Features.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace FileManager.RestApi.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods:"*")]
    public class FilesController : ApiController
    {
        Files FileOperations = new Files();
        public FileManagerServiceReference.FileManagerServiceClient Client = new FileManagerServiceReference.FileManagerServiceClient();
        // GET: api/Files
        public IEnumerable<FilesModel> Get()
        {
            throw new NotImplementedException();
        }

        // GET: api/Files/5
        public FilesModel Get(int id)
        {
            return FileOperations.Info(id);
        }
        // GET: api/Files/5
        public FilesModel Get(string filename)
        {
            return FileOperations.Info(filename);
        }
        // POST: api/Files
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Files/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Files/5
        public void Delete(int id)
        {
        }
    }
}
