﻿using Common;
using FileManager.Data;
using FileManager.Data.DomainModels;
using FileManager.Data.Tests.MockData;
using FileManager.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FileManager.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class FileManagerService : IFileManagerService
    {
        Features.Driver.Files FileOperations;
        public FileManagerService() {
            FileOperations = new Features.Driver.Files();
        }
        public FilesModel GetFileInfo(int id)
        {
            try {
                return FileOperations.Info(id);
            }
            catch (InvalidOperationException e) {
                throw new FaultException<NotFound>(HandleFault<NotFound>(e), new FaultReason(NotFound.Reason));
            }
        }


        public F HandleFault<F>(Exception exception) where F : FaultDetail {
            var fault = Activator.CreateInstance<F>();
            fault.Operation = exception.StackTrace;
            return fault;
        }
    }
}
