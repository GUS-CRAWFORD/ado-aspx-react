﻿using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FileManager.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileManagerService
    {
        [OperationContract]
        [FaultContract(typeof(NotFound))]
        FilesModel GetFileInfo(int id);

        // TODO: Add your service operations here

    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "FileManager.Service.ContractType".
    // Note: Cannot derrive from type that doesn't have [DataContract] 
    [DataContract]
    public class FaultDetail
    {
        protected string operation;
        protected string problemType;
        [DataMember]
        public string Operation { get { return operation; } set { operation = value; } }
        [DataMember]
        public string ProblemType { get { return problemType; } set { operation = value; } }
    }

    [DataContract]
    public class NotFound : FaultDetail {
        public static string Reason = "not found";
        public NotFound() {
            problemType = NotFound.Reason;
        }
    }
}
