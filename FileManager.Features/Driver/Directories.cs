﻿using FileManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using FileManager.Data.DomainModels;
using Common;
using FileManager.Data.Tests.MockData;
using FileManager.Data.Repos;

namespace FileManager.Features.Driver
{
    public class Directories : Feature<FileManagerSpoofContext>, IDirectories
    {
        public Directories()
        {
            UnitOfWork = Activator.CreateInstance<UnitOfWork<FileManagerSpoofContext>>();
            UnitOfWork.Context.Files.Add(MockFilesData.Files);
            UnitOfWork.Context.Directories.Add(MockFilesData.Directories);
            FilesRepo = (IFilesRepo)Activator.CreateInstance(typeof(SpoofFilesRepo), UnitOfWork.Context);
            DirectoriesRepo = (IDirectoriesRepo)Activator.CreateInstance(typeof(SpoofDirectoriesRepo), UnitOfWork.Context);
        }
        IFilesRepo FilesRepo;
        IDirectoriesRepo DirectoriesRepo;
        public DirectoriesModel GetDirectoryInfo(string filename)
        {
            throw new NotImplementedException();
        }

        public DirectoriesModel GetDirectoryInfo(int id)
        {
            return DirectoriesRepo.Find(id);
        }

        public IEnumerable<FilesModel> ListDirectory(string filename)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FilesModel> ListDirectory(int id)
        {
            return FilesRepo.Get().Where(f => f.Directory == id);
        }

        public DirectoriesModel MakeDirectory(string name, int inDirectory)
        {
            var lastDirectory = DirectoriesRepo.Get().LastOrDefault();
            int nextId = lastDirectory != null ? lastDirectory.Id : 0;
            var newDirectory = new DirectoriesModel
            {
                Filename = name,
                Directory = inDirectory,
                Id = nextId
            };
            DirectoriesRepo.Upsert(newDirectory);
            return newDirectory;
        }
    }
}
