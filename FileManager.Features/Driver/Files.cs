﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileManager.Data.DomainModels;
using FileManager.Data;
using FileManager.Data.Repos;
using Common;
using FileManager.Data.Tests.MockData;

namespace FileManager.Features.Driver
{
    public class Files : Feature<FileManagerSpoofContext>, IFiles
    {
        IFilesRepo FilesRepo;
        public Files () {
            UnitOfWork = Activator.CreateInstance<UnitOfWork<FileManagerSpoofContext>>();
            UnitOfWork.Context.Files.Add(MockFilesData.Files);
            UnitOfWork.Context.Directories.Add(MockFilesData.Directories);
            FilesRepo = (IFilesRepo) Activator.CreateInstance(typeof(SpoofFilesRepo), UnitOfWork.Context);
        }
        public FilesModel Info(string filename)
        {
            return FilesRepo.Get().Where(f => f.Filename == filename).Single();
        }

        public FilesModel Info(int id)
        {
            return FilesRepo.Find(id);
        }
    }
}
