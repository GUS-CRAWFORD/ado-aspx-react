﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Features
{
    public class Feature<Ctx>
        where Ctx : IGenContext, IDisposable
    {
        public UnitOfWork<Ctx> UnitOfWork;
    }
}
