﻿using FileManager.Data;
using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Features
{
    public interface IFiles
    {
        FilesModel Info(int id);
        FilesModel Info(string filename);

    }
}
