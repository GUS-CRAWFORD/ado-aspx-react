﻿using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Features
{
    public interface IDirectories
    {
        DirectoriesModel GetDirectoryInfo(int id);
        DirectoriesModel GetDirectoryInfo(string filename);
        IEnumerable<FilesModel> ListDirectory(int id);
        IEnumerable<FilesModel> ListDirectory(string filename);
        DirectoriesModel MakeDirectory(string name, int inDirectory);
    }
}
