﻿using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Data.Tests.MockData
{
    public class MockFilesData
    {
        public static List<FilesModel> Files = new List<FilesModel>
        {
            new FilesModel { Id = 1, Directory = 0, Filename = "Default.aspx" }
        };
        public static List<DirectoriesModel> Directories = new List<DirectoriesModel>
        {
            new DirectoriesModel { Id = 0, Directory = 0, Filename = "/" }
        };
    }
}
