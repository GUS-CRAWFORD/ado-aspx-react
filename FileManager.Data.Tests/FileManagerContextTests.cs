﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileManager.Data.DomainModels;
using Common;
using System.Linq;

namespace FileManager.Data.Tests
{
    [TestClass]
    public class FileManagerContextTests
    {
        private FileManagerSpoofContext FileManagerContext;
        [TestInitialize]
        public void Initialize() {
            FileManagerContext = new FileManagerSpoofContext();
        }
        [TestMethod]
        public void Instances()
        {
            Assert.IsInstanceOfType(FileManagerContext, typeof(FileManagerSpoofContext));
        }

        [TestMethod]
        public void SetReturnsList()
        {
            FileManagerContext.Files.Add(MockData.MockFilesData.Files);
            var result = FileManagerContext.GetTable<FilesModel>();
            Assert.IsTrue(Util.Object(MockData.MockFilesData.Files.First()).IsEquivalentTo(result.ToList().First()));
        }
    }
}
