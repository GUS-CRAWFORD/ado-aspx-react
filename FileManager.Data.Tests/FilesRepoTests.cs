﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;
using FileManager.Data.DomainModels;
using FileManager.Data.Repos;
using System.Collections.Generic;
using FileManager.Data.Tests.MockData;
using Moq;

namespace FileManager.Data.Tests
{
    [TestClass]
    public class FilesRepoTests
    {
        private Mock<FileManagerSpoofContext> mockFileManagerContext;
        private UnitOfWork<FileManagerSpoofContext> unitOfWork;
        private SpoofFilesRepo filesRepo;
        private SpoofDirectoriesRepo directoriesRepo;

        [TestInitialize]
        public void Initialize()
        {
            mockFileManagerContext = new Mock<FileManagerSpoofContext>();
            unitOfWork = new UnitOfWork<FileManagerSpoofContext>(mockFileManagerContext.Object);
            filesRepo = new SpoofFilesRepo(unitOfWork.Context);
            directoriesRepo = new SpoofDirectoriesRepo(unitOfWork.Context);
        }
        [TestMethod]
        public void Instances()
        {
            filesRepo.Find(1);
        }
    }
}
