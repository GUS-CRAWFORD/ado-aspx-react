﻿using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Data
{
    public interface IDirectoriesRepo : IRepo<DirectoriesModel>
    {
        
    }
}
