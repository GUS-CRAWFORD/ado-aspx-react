﻿using System;
using Common;
using FileManager.Data.DomainModels;
using System.Collections.Generic;

namespace FileManager.Data
{
    public class SpoofContext : ReflectiveModelContext<FilesSchemaModel>, IGenContext {
        public void SaveChanges()
        {
            throw new NotImplementedException();
        }
        
    }

    public class FileManagerSpoofContext : SpoofContext
    {
        public Table<FilesModel> Files { get; set; }
        public Table<DirectoriesModel> Directories { get; set; }

        public FileManagerSpoofContext() : base () { }

    }
}
