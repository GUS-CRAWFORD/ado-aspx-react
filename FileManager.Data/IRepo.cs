﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Data
{
    public interface IRepo<Type>
        where Type : class
    {
        IEnumerable<Type> Get();
        Type Find(params object[] keys);
        void Delete(Type item);
        void Upsert(Type item);
    }
}
