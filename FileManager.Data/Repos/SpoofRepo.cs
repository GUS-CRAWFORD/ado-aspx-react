﻿using Common;
using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Data.Repos
{
    public class SpoofRepo<Type> : GenRepo<Type, FileManagerSpoofContext>, IRepo<Type>
        where Type : FilesModel
    {
        protected List<Type> Collection;
        public SpoofRepo(FileManagerSpoofContext context) : base(context)
        {
            Collection = context.GetTable<Type>().ToList();
        }

        public override void Delete(Type item)
        {
            Collection.Remove(item);
        }

        public override IEnumerable<Type> Get () {
            return Collection;
        }
        public override Type Find(params object[] keys)
        {
            var key = keys.First();
            var predicate = new Predicate<Type>(
                (Type x) => (int) key == x.Id);
            return Collection.Find(predicate);
        }

        public override void Upsert(Type item)
        {
            Collection.Add(item);
        }
    }
}
