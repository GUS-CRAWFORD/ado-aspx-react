﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Common;
using FileManager.Data.DomainModels;

namespace FileManager.Data.Repos
{
    public class SpoofDirectoriesRepo : SpoofRepo<DirectoriesModel>, IDirectoriesRepo
    {
        public SpoofDirectoriesRepo(FileManagerSpoofContext context) : base(context)
        {

        }

    }
}
