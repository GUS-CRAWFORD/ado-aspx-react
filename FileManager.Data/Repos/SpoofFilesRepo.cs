﻿using Common;
using FileManager.Data.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace FileManager.Data.Repos
{
    public class SpoofFilesRepo : SpoofRepo<FilesModel>, IFilesRepo
    {
        public SpoofFilesRepo(FileManagerSpoofContext context) : base(context)
        {
        }

    }
}
