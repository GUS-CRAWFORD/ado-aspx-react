﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Data.DomainModels
{
    public class FilesSchemaModel
    {
        public FilesModel Files;
        public DirectoriesModel Directories;
    }
    public class FilesModel
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public int Directory { get; set; }
    }
    public class DirectoriesModel : FilesModel {

    }
}
