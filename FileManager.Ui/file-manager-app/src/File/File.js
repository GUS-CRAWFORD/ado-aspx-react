﻿import React, { Component } from 'react';
import $ from 'jquery';

class FileView extends Component {
	constructor(props) {
		super(props);
		this.state = {file:{Filename:'loading'}};
		
		this.req = new Promise(function (resolve, reject){
			var xhr = $.ajax({url:'http://localhost:23125/api/Files/1', crossDomain:true})
				.done(resolve)
				.fail(reject);
		});
		this.getFile();
	}
	getFile() {
		var component = this;
		this.req
			.then(function(data){
				component.setState({
					file: data
				});
			});
	}
	componentDidMount() {}
	componentWillUnmount(){}
	render() {
		return (<div>File: {this.state.file.Filename}</div>);
	}
}
export default FileView