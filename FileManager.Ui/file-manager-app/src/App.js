import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FileView from './File/File.js';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {date: new Date()};
	}
	render() {
		return (
			<FileView />
		);
	}
}

export default App;
